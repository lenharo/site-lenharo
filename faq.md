---
layout: page
title: FAQ
permalink: /faq/
---

**O que é Software Livre?**  
Por “software livre” devemos entender aquele software que respeita a liberdade
e senso de comunidade dos usuários. Grosso modo, isso significa que os usuários
possuem a liberdade de executar, copiar, distribuir, estudar, mudar e melhorar
o software. Assim sendo, “software livre” é uma questão de liberdade, não de
preço. Para entender o conceito, pense em “liberdade de expressão”, não em
“cerveja grátis”. Por vezes chamamos de “libre software” para mostrar que livre
não significa grátis, pegando emprestado a palavra em francês ou espanhol para
“livre”, para reforçar o entendimento de que não nos referimos a software como
grátis.  
fonte: [gnu.org](https://www.gnu.org/philosophy/free-sw.pt-br.html)

**O que é Debian?**  
O Projeto Debian é uma associação de indivíduos que têm como causa comum criar 
um sistema operacional livre. O sistema operacional que criamos é chamado 
Debian.  
Um sistema operacional é o conjunto de programas básicos e utilitários que 
fazem seu computador funcionar. No núcleo do sistema operacional está o kernel. 
O kernel é o programa mais fundamental no computador e faz todas as operações 
mais básicas, permitindo que você execute outros programas.  
Fonte: [debian.org](https://www.debian.org/intro/about.pt.html)
