---
layout: page
title: Links
permalink: /links/
---

Aqui listo os mais diversos links que recomendo.

* [Tabela de Aplicativos Livres](http://prism-break.org/pt/)  
* [Debian](https://www.debian.org/)  
* [CuritibaLivre](http://curitibalivre.org.br/)  
* [ICTL](https://ictl.org.br/)  
* [PapoLivre](https://papolivre.org)  
* [Free Software Foundation](https://www.fsf.org)  
* [LinuxTips](https://www.linuxtips.io)


* [100DaysOfCode](https://www.100daysofcode.com/)


Para gerar este site, utilizei [Jekyll](https://jekyllrb.com) com o tema
[Hamilton](https://github.com/ngzhio/jekyll-theme-hamilton)


**_Página em constante atualização_**
