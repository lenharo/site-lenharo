---
layout: page
title: About
permalink: /about/
---

Meu Nome é Daniel Lenharo de Souza

Profissionalmente:
 * Administrador de redes na Celepar - Companhia de Tecnologia da Informação e 
Comunicação do Paraná.

Como atividades voluntárias:
 * Atuo como Debian Developer do [projeto Debian](https://www.debian.org);
 * Membro da [Comunidade Curitiba Livre](https://curitibalivre.org.br);
 * Presidente do [ICTL - Instituto para Conservação de Tecnologias Livres](https://www.ictl.org.br/);
 * Coodernador nacional do [FLISoL Brasil - Festival Latino-americano de Instalação de Software Livre;](https://flisol.info/FLISOL2025/Brasil)
 * Co-mantenedor da [loja da comunidade Curitiba Livre](https://loja.curitibalivre.org.br).


Mini Curriculo:
Analista de Informatica, Especialista em Redes e Segurança da Informação.
Membro das comunidade Debian e Curitiba Livre. Atua como SysAdmin e busca
promover e divulgar a cultura do Software Livre.

 

Fotos para divulgação:  


<img src="/Imgs/Lenharo_01.jpg" alt="Lenharo" width="150"/>  

<img src="/Imgs/Lenharo_02.jpg" alt="Lenharo" width="150"/>  

<img src="/Imgs/Lenharo_03.jpg" alt="Lenharo" width="150"/>  

<img src="/Imgs/Lenharo_04.jpg" alt="Lenharo" width="150"/>  

<img src="/Imgs/Lenharo_05.jpg" alt="Lenharo" width="150"/>  

<img src="/Imgs/Lenharo_06.jpg" alt="Lenharo" width="150"/>  

