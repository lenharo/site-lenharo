---
layout: page
title: Palestras
permalink: /talks/
---

Nesta página você vai encontrar a lista das minhas palestras, onde apresentei,
e arquivo para download do slide mais atualizado.
Obs: Caso tenha sido gravada, buscarei linkar a apresentação também.

### > Debian

**Debian - Projeto e Comunidade**  
2024, Abril: MiniDebConf BH  
2024, Agosto: Debian Day Curitiba  
[SLIDES](/Files/Slides/Debian-Projeto_Comunidade.odp)


**How is to starting packaging...**  
2020, Agosto: DebConf20 / Online  
[SLIDES](/Files/Slides/DC20-Lenharo_How_is_to_start_packaging.odp)   


**l10n-portuguese... How we are working? And where we want to go?**  
2020, Agosto: DebConf20 / Online  
[SLIDES](/Files/Slides/DC20-l10n-portuguese-How_we_are_working_And_where_we_want_to_go.odp)  

**Debian - O Sistema Universal**  
2020, Maio: #FiqueEmCasaUseDebian / Online  
2017, Setembro: IX FTSL / Curitiba - PR  
2017, Maio: Cryptorave /  São Paulo - SP  
2017, Fevereiro: Campus Party / São Paulo - SP  
2016, Novembro: Semana Acadêmia Informática UFPR. UFPR Litoral / Matinhos - PR  
2016, Maio: Circuito Curitiba de Software Livre - 7 Etapa. Unicuritiba /
Curitiba - PR  
2015, Setembro: Circuito Curitiba de Software Livre - 4 Etapa. OPET / Curitiba- PR  
2015, Agosto: Circuito Curitiba de Software Livre - 3 Etapa. SPEI / Curitiba - PR  
[VIDEO](https://youtu.be/K_BIO7c60oI)  


### > Software Livre

**Sendo SysAdmin com Software Livre**  
2016, Novembro: Semana Acadêmia Informática UFPR. UFPR Litoral / Matinhos - PR  
2016, Outubro: LATINOWARE - Conferência LatinoAmericana de Software Livre / Foz do Iguaçu - PR  
2016, Julho: FISL - Forum Internacional de Software Livre / Porto Alegre - RS  


**Como ganhar dinheiro com Software Livre**  
_Palestra descontinuada_  
2015, Setembro: VII FTSL. - UTFPR / Curitiba - PR  
2015, Novembro: I Semana Acadêmica de Informática da Universidade Federal do Paraná - UFPR / Curitiba - PR  

### > ICTL

**LightTalk sobre o ICTL**  
2024, Abril: MiniDebConf BH  
[SLIDES](/Files/Slides/ICTl.odp)  




_Página em constante atualização_
