---
layout: page
title: 100 days
permalink: /100doc/
---

## 100days of code 2025

I will use this page to write the days of the [100 days of code challenge.](https://www.100daysofcode.com/)
For this year of 2025, I will study backend in python.

### Day01
##### January 1st

Today, i start to read the book "Python for developers 2nd edition". 

I used this book, to review some basic structure like:
  - functions;  
  - I/O files;  
  - Class;  
  - Decoratores;  
  - Automated testing.


### Day02
##### January 2nd

Today, I continued reading the book, doing exercises on:
 - Inheritance;
 - Operator overloading;
 - Serialization;

It was my sister's birthday, so I could only dedicate approximately 90 minutes
to it.

### Day03
##### January 3rd

Today my focus was in thread. I spend my time making exercises and reading
documentation (and the book).


### Day04
##### January 4th


Today, i read about serialization and Data Bases (ZOPE and SQL). 

I spend some time to make some exercises that i found on book.

### Day05
##### January 5th

Today, I spend a lot of time doing exercises on threads.
For me, this is a good topic that needs to be studied very carefully to
assimilate the content.  

IMHO, threads are very important for the backend.


