---
layout: post
title: Richard Stallman backs to FSF board
tags: [Free Software, fsf, Stallman, Debian]
---

Hello everyone!!
During the last weekend, [LibrePlanet](https://libreplanet.org/2021/) took place
online and during one of the talks, there was the announcement of the return of
the creator of the Free Software movement Richard M. Stallman to the FSF board.
Following these announcements, several entities began issuing notes against this
return decision.
Myself, as an activist and defender of the movement, am happy with this decision.
We all know that RMS is not an easy person to relate to, but the way in which he 
left in 2019, in my view, was very unfair to the whole history and his importance
for Free Software and the FSF itself.
With this decision, I believe, that it can be done given the opportunity to
continue defending the Free Software movement, and if anyone deems it necessary in 
the future for its departure, it can be done transparently and consistently.
It is obvious that I say this, only as an observer at a distance from the facts,
without having the necessary internal knowledge that led to this re-instatement of
Stallman to the direction of the FSF. Let us wait for the video promised with the
due announcement.
Meanwhile, I increased my contribution to the entity, to show my satisfaction with
the decision.


![stallman](/Imgs/stallman.jpg)
