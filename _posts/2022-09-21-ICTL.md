---
layout: post
title: ICTL - Nova Diretoria
tags: [Free Software, ICTL, Software Livre, Eventos]
---
# [ICTL](https://www.ictl.org.br) #

**Fundado em 2018**, O ICTL – Instituto para Conservação de Tecnologias Livres – é uma associação sem fins lucrativos cujo objetivo é promover a divulgação, o desenvolvimento e a defesa do Software Livre. O ICTL desenvolve ações para o avanço do Software Livre em geral, e fornece uma estrutura institucional para projetos de Software Livre, de forma que eles possam receber doações, realizar despesas, e manter recursos.

Desde sua fundação o presidente era o Antonio Terceiro. No ultimo mês de julho, aconteceu uma assembleia geral para eleição da nova composição de diretoria, na qual eu assumi como presidente da entidade.

O Objetivo, ao assumir é dar continuidade aos projetos em andamento, e fomentar novos projetos pelo Brasil!

Se você conhece algum projeto que precise de algum apoio, nos procure!

contato@ictl.org.br