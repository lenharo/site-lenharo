---
layout: post
title: Gitlab Commit virtual 2021
tags: [Free Software, GITLAB, DEBIAN, EVENTOS]
---

 Hoje começou o evento **Gitlab Commit Virtual** edição 2021. O evento
 acontecerá hoje e amanhã (03 e 04 de agosto de 2021) com muitas palestras e
 atividades para ajudar as pessoas a aprimorar seu conhecimento em DevOps.
 Vale muito a pena particiar!  
 Acesse o [site do evento](https://gitlabcommitvirtual2021.com/) para ficar
 por dentro de tudo que acontec{ecrá,e,eu}.  

![GitlabCommit](/Imgs/gitlabcommit.png)




![Eu no GitlabCommit](/Imgs/gitlab2021_eu.jpg)
