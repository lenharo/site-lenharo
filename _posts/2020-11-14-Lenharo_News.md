---
layout: post
title: Lenharo News
tags: [Pessoal, Informações]
---

Há algum tempo estava querendo voltar a estudar de forma "oficial". Estava
acompanhando diversas pós-graduações oferecidas, mas nenhuma me interessava.
Até que recentemente a [UTFPR](http://portal.utfpr.edu.br/) abriu uma pós em
[Segurança Cibernética](http://cesc.utfpr.edu.br/) com uma grade bem
interessante. Como tenho estudado há algum tempo esta área, resolvi me
inscrever.  

Durante o ultimo mês de outubro, as aulas iniciaram, com aulas básicas de
redes. Ainda não há nenhuma novidade, em relação ao que já estudei na graduação
e nas demais pós que fiz, mas a expectativa é boa para os demais módulos que
virão.  

Durante este mês de Outubro e Novembro, a [CELEPAR](http://www.celepar.pr.gov.br/)
me proporcionou a participação no curso oficial de EXIM para DPO da GDPR. Um
curso muito interessante, que ajuda em muito na visão para o cuidado dos dados
pessoais. Agora quero focar os estudos e juntar um cascalho para fazer as
provas da certificação!  

No Debian, lançamos a [MiniDebConf Online](https://mdcobr2020.debian.net/) que
acontecerá nos dias 28 e 29 de Novembro Online. Fizemos a chama de palestras,
seleção das palestras e montagem da grade.  
Além das lives do Debian Brasil na terça, tenho participado das reuniões do
pessoal do [Debian Brasilia](https://debianbrasilia.org/), já que o grupo de
Curitiba não se movimenta para fazermos reuniões nem nada. Apesar que o povo de
BSB é bem receptivo e as reuniões são interessantes de participar!  

Estou fazendo alguns planos de utilizar mais o canal da Twitch para fazer
alguns estudos técnicos, e depois compilar a gravação para o youtube. Vendo
ainda como fazer isso e ver se meu note mais aguentar, coitado tá abrindo o
bico já.


Bom é isso! só queria atualizar mesmo!

Hasta la vista!!
