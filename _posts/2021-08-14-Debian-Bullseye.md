---
layout: post
title: Lançado a versão 11 do Debian!
tags: [Free Software, DEBIAN, BULLSEYE]
---

 Hoje(14 de Agosto de 2021) é um dia de festa para toda a comunidade em torno do
 [Projeto Debian](https://www.debian.org). Está sendo realizado o lançamento
 da nova versão estável do Debian! A Versão 11 está disponível


*Após 2 anos, 1 mês e 9 dias de desenvolvimento, o projeto Debian 
orgulhosamente apresenta sua nova versão estável (stable) 11 (codinome
**bullseye**), a qual será suportada pelos próximos 5 anos graças ao esforço 
combinado da [Equipe de Segurança](https://security-team.debian.org) e da
[Equipe de Suporte de Longo Prazo](https://wiki.debian.org/LTS) do Debian.*


*O Debian 11 bullseye é distribuído com diversas aplicações e ambientes de área
de trabalho. Entre outros, estão incluídos os seguintes ambientes de área de
trabalho:
 - Gnome 3.38,
 - KDE Plasma 5.20,
 - LXDE 11,
 - LXQt 0.16,
 - MATE 1.24,
 - Xfce 4.16.


 Este lançamento contém 11.294 novos pacotes de um total de 59.551
pacotes, junto à uma redução significativa de 9.519 pacotes que foram marcados
como "obsoletos" e removidos. Foram 42.821 pacotes atualizados e 5.434
pacotes permaneceram sem alterações.


O **bullseye** é nosso primeiro lançamento que fornece um núcleo Linux com 
suporte ao sistema de arquivos exFAT e que permite por padrão utilizá-lo para
montar sistemas de arquivo exFAT. Consequentemente, não é mais requerido usar a
implementação sistema-de-arquivo-no-espaço-de-usuário(a) através do pacote 
exfat-fuse. As ferramentas para criar e verificar um sistema de arquivos
exFAT são fornecidas no pacote exfatprogs.


Impressoras mais modernas são capazes de utilizar impressão e escaneamento sem
drivers, sem a necessidade de drivers de fabricantes específicos (frequentemente
não livres). 

O bullseye apresenta um novo pacote, ipp-usb, que usa o protocolo IPP-sobre-USB,
neutro em relação a fabricantes e suportado por muitas impressoras modernas.
Isto permite que um dispositivo USB seja tratado como um dispositivo de rede. A
infraestrutura oficial e sem driver SANE é fornecida por sane-escl em libsane1,
que usa o protocolo eSCL.


O Systemd no bullseye ativa sua funcionalidade de registro (journal)
persistente, por padrão, com uma salvaguarda implícita para armazenamento
volátil. Isto permite que usuários(as) que não estejam amparados(as) por
recursos especiais possam desinstalar daemons tradicionais de log e
trocá-los usando somente o journal do Systemd.


A equipe Debian Med tem participado da luta contra o COVID-19
empacotando software para pesquisa e sequenciamento do vírus, e lutando
contra a pandemia com ferramentas usadas na epidemiologia. Seu  trabalho 
junto à equipe de Garantia de Qualidade e Integração Contínua é crítica para
os resultados reproduzíveis e consistentes que são exigidos pelas ciências.


Uma gama de aplicações críticas de desempenho agora se beneficiam do SIMD
Everywhere. Para instalar pacotes mantidos pela equipe Debian Med, instale os
metapacotes name med-\*.


Chinês, japonês, coreano e muitos outros idiomas agora têm um novo método de
entrada Fcitx 5, que é o sucessor do popular Fcit4 no buster. Esta nova versão
tem um suporte muito melhor de extensão ao Wayland (gerenciador de exibição
padrão).


O **Debian 11 bulleye** inclui muitos pacotes de software atualizados (mais
de 72% de todos os pacotes da versão anterior), tais como:

 - Apache 2.4.48
 - BIND DNS Server 9.16
 - Calligra 3.2
 - Cryptsetup 2.3
 - Emacs 27.1
 - GIMP 2.10.22
 - GNU Compiler Collection 10.2
 - GnuPG 2.2.20
 - Inkscape 1.0.2
 - LibreOffice 7.0
 - kernel Linux série 5.10
 - MariaDB 10.5
 - OpenSSH 8.4p1
 - Perl 5.32
 - PHP 7.4
 - PostgreSQL 13
 - Python 3, 3.9.1
 - Rustc 1.48
 - Samba 4.13
 - Vim 8.2
 - mais de 59.000 pacotes de software prontos para usar, construídos a partir 
   de 29.000 pacotes-fonte.


Com esta ampla seleção de pacotes e seu vasto e tradicional suporte a
arquiteturas, o Debian mais uma vez mantém-se verdadeiro a seu objetivo de ser
'o sistema operacional universal'. É adequado para muitos usos: de sistemas
desktops a netbooks; de servidores de desenvolvimento a sistemas em cluster; e
para servidores de bancos de dados, web e armazenagem. Ao mesmo tempo,
esforços adicionais de garantia de qualidade, como testes de instalação
automática e atualização para todos os pacotes do repositório Debian, asseguram
que o bullseye alcance as elevadas expectativas que os(as) usuários(as)
têm de uma versão Debian estável.



Um total de nove arquiteturas são suportadas:
 1. AMD64 -> 64-bit PC / Intel EM64T / x86-64;
 2. i386-> 32-bit PC / Intel IA-32;
 3. ppc64el -> 64-bit little-endian Motorola/IBM PowerPC;
 4. s390x -> 64-bit IBM S/390;
 5. armel -> para ARM;
 6. armf -> armhf para hardware 32-bit mais antigos e mais recentes,
 7. Arm64 -> arm64 para a arquitetura 64-bit,
 8. Mipsel -> MIPS (little-endian) para hardware 32-bit
 9. mips64el -> mips64el para hardware 64-bit little-endian.


Sobre o Debian


O Debian é um sistema operacional livre, desenvolvido por
milhares de voluntários(as) de todo o mundo que colaboram através da
Internet. Os pontos fortes do projeto Debian são sua base de voluntários(as),
sua dedicação ao Contrato Social do Debian e ao Software Livre, e seu
compromisso em fornecer o melhor sistema operacional possível. Esta nova
versão é mais um passo importante nessa direção.



![Debian Bullseye](/Imgs/posts/debian11.jpg)


A todos os times do Debian que trabalharam arduamente para este lançamento, 
meu muito obrigado e parabéns!!!


*Fonte das informações: Anúncio de lançamento Debian 11 - Debian.org
