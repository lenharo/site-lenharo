---
layout: post
title: Using Let’s Encrypt to enable SSL on your site
tags: [Free Software, SSL]
---

Hello,

This post is a mix of tip and tutorial.

I as moving some websites from wordpress to local VPS. After i moved then, i
realized that i need to setup SSL to then.

Long time ago i have used Let's Encrypt to provide it, and i remember that it
was not a easy task.

So i tryed again, and now it's so easy that i consider to write about this.

[Debian Wiki](https://wiki.debian.org/LetsEncrypt) has everything that you
need to setup it.

Basically you need to do:

-Setup your website - using apache or nginx;
-install certbot package; (apt install certbot);
-install plugin for Certbot; (python3-certbot-{apache|nginx]
-Create and setting your certificate; (sudo certbot --{apache|nginx} -d <domain> --post-hook "/usr/sbin/service {apache2|nginx} restart"

and done!

Piece of cake!

