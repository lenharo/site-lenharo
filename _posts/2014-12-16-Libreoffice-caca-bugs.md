---
layout: post
title: Preparare-se para a Temporada de Caça-Bugs do LibreOffice 4.4
tags: [LibreOffice, cacabugs]
---

 A The Document Foundation (TDF) anuncia a segunda sessão de caça-bugs do 
 LibreOffice 4.4, que ocorrerá de 19 a 21 de Dezembro de 2014, imediatamente 
 após o lançamento do primeiro “release candidate” do software, cujo lançamento
 final está previsto para fins de Janeiro de 2015.

![Caca-Bugs-44](/Imgs/caca-bugs-44.jpg)

Saiba tudo em:
[http://blog.pt-br.libreoffice.org/2014/12/16/prepare-se-para-a-temporada-de-caca-bugs-do-libreoffice-4-4/](http://blog.pt-br.libreoffice.org/2014/12/16/prepare-se-para-a-temporada-de-caca-bugs-do-libreoffice-4-4/)

