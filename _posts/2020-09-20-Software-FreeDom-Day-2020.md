---
layout: post
title: SFD2020 - Software Freedom Day
tags: [Free Software, SFD, SFD2020]
---
 Ontem, 19 de Setembro de 2020, foi comemorado o dia da Liberdade do Software,
 um dia que *EU* considero muito importante para quem ainda acredita neste
 movimento. Devido ao momento de pandemia, que ainda estamos passando, não foi
 possível fazermos encontros presenciais para celebrar este dia tão especial.
 Porém, não poderiamos deixar esta data de lado, não é mesmo? por isso, algumas
 resistentes pessoas, através de seus canais na internet, realizaram atividades
 para manter acesas as chamas so Software Livre.  
 O [Paulo Santana](http://phls.com.br/) teve a iniciativa de reunir na
 [wiki do SFD](http://wiki.softwarefreedomday.org/2020/Brazil/Online), a lista
 dos canais participantes para que todos pudessem acompanhar as atividades!.  

 Podemos na [Wiki](http://wiki.softwarefreedomday.org/2020) que alguns outros
 países também tiveram atividades e realizaram suas atividades. Confesso, que
 acabei não conseguindo acompanhar estas atividades, mas ao longo da semana,
 vou buscar saber sobre elas.  

 Quero abrir um pequeno parenteses, para dizer que nem tudo foram flores, em
 várias falas, foi notório que o termo Open Source está enraizado em várias
 pessoas, um movimento que ao meu ver(e sei que vários outros compartilham este
 pensamento) ajudou a enfraquecer a filosofia e cultura do Software Livre.


Minha única atividade, foi a participação em uma
[mesa redonda](https://youtu.be/FniGoZ-BeS4), promovida pela Comunidade Fedora
Brasil. Nesta Mesa falamos um pouco do SL nos ultimos anos no Brasil e o que
esperamos para os próximos anos. Agradeço o convite!


Continuo acreditando no Software Livre, e semPRe lutarei para que o Software
Livre esteja inserido no máximo possível de contextos!

 ![SFD2020](/Imgs/sfd2020.png)
