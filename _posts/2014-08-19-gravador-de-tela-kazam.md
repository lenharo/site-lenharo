---
layout: post
title: Gravador de Tela
tags: [Free Software, App, Tips]
---


 Estava eu, procurando algum programa realizar a gravação de vídeos tutoriais.

 Testei diversas opções, porém a minha escolha foi o Kazam. Programa muito 
 leve, tem a capacidade de efetuar gravação de multiplas telas, pode-se escolher
 uma determinada área ou janela. A capaticação do audio é excelente, e o 
 resultado final é um arquivo com um tamanho reduzido.

 Para Debian é possivel encontrar nos repositórios.

 O Kazam é um aplicativo muito bom para quem grava vídeos para o YouTube e gosta
 de qualidade. Diferente de vários outros aplicativos, ele é muito completo e 
 sua interface é bem intuitiva.

É um aplicativo que tem tradução para português do Brasil, o que facilita muito
para quem deseja fazer isto!
