---
layout: post
title: DebConf 22, Prizren Kosovo
tags: [Free Software, DC22, DEBIAN, EVENTOS]
---

Depois de uma semana sendo realizada a DebCamp - dias para os contribuidores
do projeot trabalharem em seus projetos, e a organização ter tempo de realizar
os ajustes finais para a DebConf. A [DebConf22](https://debconf22.debconf.org)
assim como a DebCamp, acontecerá na cidade de Prizen em Kosovo. O espaço
escolhido para abrigar as pessoas do evento e as atividades, foi o ITP -
**Inovation and trainin Park **.


Assim como acontece todos os anos, a DebConf terá as principais atividades
sendo transmitidas através do site oficial do evento! Participe e acompanhe!

 A [programação completa](https://debconf22.debconf.org/schedule/) e a lista de
 [palestras](https://debconf22.debconf.org/talks/) pode ser consultado no site.  


![Logo DebConf22](/Imgs/DebConf22.png)

Debian - Sistema Operacional Universal!

Use Software Livre

