---
layout: post
title: My Free Software Activities in August 2020
tags: [Free Software, Software Livre, Debian]
---
 A long time, i want to start to do this blog post. But i didn't do before,
because my blog was not good to update, and usually don't have a lot 
contributions to tell about.  
 During August, i did some Debian contributions, specially at DebConf.  

 * Help to review DebConf talks;  
 * Volunteer from the DebConf video team;  
 * DebConf t-shirst team;  
 * Debian Brasil lives at Youtube channel;

 The next report i want to have more things to show!.  


![SL](/Imgs/SL.svg)


Thanks for your reading!!
