---
layout: post
title: Lenharo becoming alligator
tags: [Pessoal, Informações]
---

É hoje é um dia de muita alegria para mim! Depois de muita espera, chegou meu
dia e consegui tomar a primeira dose da vacina contra a COVID19.  
Esses dias de espera não tem sido fáceis para ninguem... muitas vidas perdias
no mundo inteiro. Quantas famílias desfeitas por causa de um vírus? Pior
ainda, saber que muitas vidas poderiam ter sido salvas com ações mais efetivas
de quem está a frente das decisões!

Continuem acreditando na Ciência, somente ela pode salvar nossas vidas!!!
