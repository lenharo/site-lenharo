---
layout: post
title: Free Software Contribuitions
tags: [Free Software, Software Livre]
---

Since [DebConf](https://debconf22.debconf.org) i attended in Kosovo, i put a step down at my contribuitions to Debian. I Have to focus my attention in some professional stuffs and as my employer did not support contributions during working hours, i have to step down.

Even with, i had time to attended [Brazil Campus Party](https://brasil.campus-party.org), that happen in São Paulo. At this event, we had a table to promote Debian, where we received people to tell them about what Debian is, how to contribute, fix some bug, help about installations and subjects that people needed. We take the opportunity to gave some swags to people that was attending.

i'm using some time at this month to upload some packages that i maintain.

I hope to have more time next year even to have more contributions, even to attend more events.

