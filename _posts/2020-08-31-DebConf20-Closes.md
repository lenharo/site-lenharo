---
layout: post
title: DebConf20 Acabou!
tags: [Free Software, Software Livre, Debian, Eventos]
---

 Após uma semana de muita atividades, tivemos o encerramento da 
[DebConf20](https://debconf20.debconf.org/) - Conferencia anual do Projeto Debian.
Como disse no [post anterior](https://www.sombra.eti.br/2020/08/19/DebConf20.html),
deveria ter acontecido, este evento deveria ter acontecido em Israel. Porém em menos
de 2 menos, o time da DebConf conseguir montar uma estrutura e tivemos um evento 
incrível!  
 Quem não teve o privilégio de acompanhar durante a realização do evento, os vídeos
estão sendo disponibilizados na página de [vídeos do Debian](https://video.debian.net).  
 Muitas atividades se destacaram no evento, mas vou destacar as que eu vi que mais
me despertaram atenção.

 * Doing Thing /together/;  
 * Empaquetamiento de Debian: de novata a novato;  
 * Bits From DPL;  
 * Experiment about Debian's bug tracking front-end;  
 * What's new in the Linux kernel (and what's missing in Debian);  
 * Introduction to GNU/Linux in Education;  
 * My phone runs Debian - and it does phone calls!;  
 * Debian Academy: Another way to share knowledge about Debian;  
 * Leadership in Debian BoF/Panel;  
 * Running autopkgtest for your package;  
 * When We Virtualize the Whole Internet;  
 * Local Teams;  
 * Closing session: So long, and thanks for all the .debs!;  


 Fazer parte da realização deste evento, sem dúvida é algo especial. Espero, contribuir
muito mais no futuro, mas sinto que tenho ajudado num nivel bem maior que antigamente.


Esta é a foto oficial do Evento!

![foto do evento](/Imgs/dC20_group_s.jpg)


Até a DebConf21!!
Espero ver todos em Haifa!
