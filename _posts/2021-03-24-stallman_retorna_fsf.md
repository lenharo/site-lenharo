---
layout: post
title: Richard Stallman Retorna a diretoria da FSF
tags: [Software Livre, fsf, Stallman, Debian]
---

Olá a todos!!
Durante o último fim de semana, aconteceu o [LibrePlanet](https://libreplanet.org/2021/)
de forma online e durante uma das palestras, houve o anúncio do retorno do
criador do movimento do Software Livre Richard M. Stallman à diretoria da FSF.
Após este anúncio, várias entidades começaram a emitir notas contra esta
decisão do retorno.  
Eu, Daniel Lenharo, como ativista e defensor do movimento fico feliz com esta
decisão.  
Todos sabemos que RMS não é uma pessoa fácil de se relacionar, mas a forma com
que houve o afastamento dele em 2019, ao meu ver foi muito injusto com toda a
história e a importância dele para o Software Livre e a própria FSF.  
Com esta decisão, acredito, que possa ser feita dado a oportunidade de
continuidade da defesa do movimento do Software Livre, e se alguém julgar
necessário no futuro pela saída dele, que possa ser feita de forma transparente
e com coerencia.  
É obvio que falo isso, apenas como um observador a distância dos fatos, sem ter
os devidos conhecimentos internos que levaram a esta recondução do Stallman à
direção da FSF. Aguardemos o vídeo prometido com o devido anúncio.  
Enquanto isso, realizei o aumento da minha contribuição para a entidade, para
mostrar minha satisfação com a decisão.

 ![stallman](/Imgs/stallman.jpg)
